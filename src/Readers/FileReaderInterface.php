<?php

namespace Rodw\FileManipulator\Readers;

interface FileReaderInterface 
{
    /**
     * Get the first line that contains the given string
     *
     * @param $string
     * @return mixed
     */
    public function getFirstLineFor($string);

    /**
     * Get all the lines that contain the given string
     *
     * @param $string
     * @return array
     */
    public function getAllLinesFor($string);

    /**
     * Get the last line that contains the given string
     *
     * @param $string
     * @return mixed
     */
    public function getLastLineFor($string);

    /**
     * Get the first position that contains the given string
     *
     * @param $string
     * @return mixed
     */
    public function getFirstPositionFor($string);

    /**
     * Get all the positions for the given string
     *
     * @param $string
     * @return mixed
     */
    public function getAllPositionsFor($string);

    /**
     * Get the last position that contains the given string
     *
     * @param $string
     * @return mixed
     */
    public function getLastPositionFor($string);
}