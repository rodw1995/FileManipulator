<?php

namespace Rodw\FileManipulator\Readers;


class ClassReader extends BaseReader implements ClassReaderInterface
{
    /**
     * @param $filePath
     * @throws \Exception
     */
    public function read($filePath)
    {
        parent::read($filePath);

        if (is_null($this->getClassStartPosition())) {
            throw new \Exception('Cannot find a class in this file (' . $filePath . ')');
        }
    }

    /**
     * Returns the line of the start bracket ({) of the class
     *
     * @return integer
     */
    public function getClassStartLine()
    {
        return $this->positionToLineNumber($this->getClassStartPosition());
    }

    /**
     * Returns the line of the end bracket (}) of the class
     * @return int
     * @throws \Exception
     */
    public function getClassEndLine()
    {
        $pos = strrpos($this->content, '}');

        if ($pos !== false) {
            return $this->positionToLineNumber($pos);
        }

        throw new \Exception('Cannot find end of class in file ' . $this->file);
    }

    /**
     * Returns the line of the start bracket ({) of the given method
     *
     * @param string $method
     * @return int
     * @throws \Exception
     */
    public function getMethodStartLine($method)
    {
        if ($this->methodExists($method)) {
            return $this->positionToLineNumber($this->getMethodStartPosition($method));
        }

        throw new \Exception('Cannot find start of method "' . $method . '" in file ' . $this->file);
    }

    /**
     * Returns the line of the end bracket (}) of the given method
     *
     * @param string $method
     * @return int
     * @throws \Exception
     */
    public function getMethodEndLine($method)
    {
        if ($this->methodExists($method)) {
            preg_match('#\{((?>[^\{\}]+)|(?R))*\}#x', $this->content, $matches, PREG_OFFSET_CAPTURE, $this->getMethodStartPosition($method));

            if (!empty($matches)) {
                $match = $matches[0];

                return $this->positionToLineNumber(strlen($match[0]) - 1 + $match[1]);
            }
        }

        throw new \Exception('Cannot find end of method "' . $method . '" in file ' . $this->file);
    }

    /**
     * Return the line of the last use statement in the class
     *
     * @return integer
     */
    public function getLastUseLine()
    {
        $offset = -(strlen($this->content) - $this->getClassStartPosition());
        $pos = strrpos($this->content, 'use', $offset);

        if ($pos !== false) {
            return $this->positionToLineNumber($pos);
        }

        // No use line in this class
        return null;
    }

    /**
     * Check if the method exists in the file
     *
     * @param string $method
     * @return bool
     */
    public function methodExists($method)
    {
        return !is_null($this->getMethodStartPosition($method));
    }

    /**
     * Get the start position of the class open bracket ({)
     *
     * @return int|null
     */
    private function getClassStartPosition()
    {
        $pos = strpos($this->content, '{');

        if ($pos !== false) {
            return $pos;
        }

        return null;
    }

    /**
     * Get the start position of the open bracket ({) of the given method
     *
     * @param string $method
     * @return int|null
     */
    private function getMethodStartPosition($method)
    {
        $content = $this->content;
        preg_match('/function\s+' . $method . '\(/', $content, $matches, PREG_OFFSET_CAPTURE, $this->getClassStartLine());

        if (!empty($matches)) {
            $bracketPos = strpos($content, '{', $matches[0][1]);

            if ($bracketPos !== false) {
                return $bracketPos;
            }
        }

        return null;
    }
}