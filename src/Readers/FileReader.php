<?php

namespace Rodw\FileManipulator\Readers;


class FileReader extends BaseReader implements FileReaderInterface
{
    public function getFirstLineFor($string)
    {
        return $this->getAllLinesFor($string)[0];
    }

    public function getAllLinesFor($string)
    {
        $positions = $this->getAllPositionsFor($string);
        $lines = [];

        foreach ($positions as $position) {
            $lines[] = $this->positionToLineNumber($position);
        }

        return $lines;
    }

    public function getLastLineFor($string)
    {
        $allLines = $this->getAllLinesFor($string);
        return $allLines[count($allLines) - 1];
    }

    public function getFirstPositionFor($string)
    {
        $positions = $this->getAllPositionsFor($string);
        return $positions[0];
    }

    public function getAllPositionsFor($string)
    {
        $lastPos = 0;
        $positions = [];

        while (($lastPos = stripos($this->content, $string, $lastPos)) !== false) {
            $positions[] = $lastPos;
            $lastPos = $lastPos + strlen($string);
        }

        return $positions;
    }

    public function getLastPositionFor($string)
    {
        $positions = $this->getAllPositionsFor($string);
        return $positions[count($positions) - 1];
    }
}