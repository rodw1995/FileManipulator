<?php

namespace Rodw\FileManipulator\Readers;


class PHPArrayFileReader extends BaseReader implements PHPArrayFileReaderInterface
{
    public function read($filePath)
    {
        parent::read($filePath);

        if (is_null($this->getReturnStartPosition())) {
            throw new \Exception('Cannot find a class in this file (' . $filePath . ')');
        }
    }

    /**
     * Get the start line of the return statement
     *
     * @return integer
     */
    public function getReturnStartLine()
    {
        return $this->positionToLineNumber($this->getReturnStartPosition());
    }

    /**
     * Get the end line of the return statement
     *
     * @return mixed
     */
    public function getReturnEndLine()
    {
        return $this->positionToLineNumber($this->getReturnEndPosition());
    }

    /**
     * Return the start position of the return statement
     *
     * @return bool|int
     *
     * @NOTE Easy check, maybe replace it with a fancy regex
     */
    private function getReturnStartPosition()
    {
        return strpos($this->content, 'return');
    }

    /**
     * Return the position of the end of the return statement
     *
     * @return bool|int
     *
     * @NOTE Easy check, maybe replace it with a fancy regex
     */
    private function getReturnEndPosition()
    {
        return strrpos($this->content, ';');
    }
}