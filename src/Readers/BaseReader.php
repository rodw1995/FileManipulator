<?php

namespace Rodw\FileManipulator\Readers;


abstract class BaseReader
{
    /**
     * @var string
     */
    protected $file;

    /**
     * @var string
     */
    protected $content;

    /**
     * @param $filePath
     * @throws \Exception
     */
    public function read($filePath)
    {
        $this->file = $filePath;
        $this->content = file_get_contents($filePath);

        if ($this->content === false) {
            throw new \Exception('Cannot read file ' . $filePath);
        }
    }

    /**
     * Get of the given position the associated line number
     *
     * @param $pos
     * @return int
     */
    protected function positionToLineNumber($pos)
    {
        preg_match_all("/((\r?\n)|(\r\n?))/", $this->content, $matches, PREG_OFFSET_CAPTURE);

        if (empty($matches[0])) {
            // No matches, only one line so..
            return 1;
        }

        $prevLinePos = 0;
        $lastLineNumber = 0;
        foreach ($matches[0] as $lineNumber => $match) {
            $lastLineNumber++;
            if ($pos > $prevLinePos && $pos < $match[1]) {
                return $lastLineNumber;
            }
        }

        // It will be the last line
        return $lastLineNumber + 1;
    }
}