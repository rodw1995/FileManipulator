<?php
namespace Rodw\FileManipulator\Readers;

use Exception;

interface ClassReaderInterface
{
    /**
     * Returns the line of the start bracket ({) of the class
     *
     * @return integer
     */
    public function getClassStartLine();

    /**
     * Returns the line of the end bracket (}) of the class
     *
     * @return integer
     * @throws Exception
     */
    public function getClassEndLine();

    /**
     * Returns the line of the start bracket ({) of the given method
     *
     * @param string $method
     * @return integer
     * @throws Exception
     */
    public function getMethodStartLine($method);

    /**
     * Returns the line of the end bracket (}) of the given method
     *
     * @param string $method
     * @return integer
     * @throws Exception
     */
    public function getMethodEndLine($method);

    /**
     * Return the line of the last use statement in the class
     *
     * @return integer
     */
    public function getLastUseLine();
}