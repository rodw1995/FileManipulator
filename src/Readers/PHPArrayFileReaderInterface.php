<?php
namespace Rodw\FileManipulator\Readers;

interface PHPArrayFileReaderInterface
{
    /**
     * Get the start line of the return statement
     *
     * @return integer
     */
    public function getReturnStartLine();

    /**
     * Get the end line of the return statement
     *
     * @return mixed
     */
    public function getReturnEndLine();
}