<?php

namespace Rodw\FileManipulator;


use Symfony\Component\Filesystem\Filesystem;

class FileManipulator implements FileManipulatorInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;
    private $content;
    private $filePath;
    private $lines;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function setFile($filePath)
    {
        $this->content = file_get_contents($filePath);
        $this->filePath = $filePath;
        $this->setLines($this->content);

        return $this;
    }

    public function add($newContent, $afterLine = null)
    {
        if (is_null($afterLine) OR !$this->lineExists($afterLine)) {
            $this->lines[] = $newContent;
        } else {
            array_splice($this->lines, $afterLine, 0, $newContent);
        }

        return $this;
    }

    public function remove($line)
    {
        if ($this->lineExists($line)) {
            unset($this->lines[$line]);
        }

        return $this;
    }

    public function change($line, $newContent)
    {
        if ($this->lineExists($line)) {
            $this->lines[$line] = $newContent;
        }

        return $this;
    }

    public function lineExists($line)
    {
        return isset($this->lines[$line]);
    }

    public function getFileContent()
    {
        $content = '';

        foreach ($this->lines as $line) {
            $content .= $line . "\r\n";
        }

        return rtrim($content);
    }

    /**
     * Save the file
     */
    public function save()
    {
        $this->filesystem->dumpFile($this->filePath, $this->getFileContent());
    }

    private function setLines($content)
    {
        $this->lines = preg_split("/((\r?\n)|(\r\n?))/", $content);
    }
}