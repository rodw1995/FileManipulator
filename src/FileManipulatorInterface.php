<?php
namespace Rodw\FileManipulator;

interface FileManipulatorInterface
{
    /**
     * Set the file to manipulate
     *
     * @param $filePath
     * @return FileManipulatorInterface
     */
    public function setFile($filePath);

    /**
     * Add a new line after the given line number
     *
     * @param $newContent
     * @param $afterLine
     * @return FileManipulatorInterface
     */
    public function add($newContent, $afterLine = null);

    /**
     * Remove a line
     *
     * @param $line
     * @return FileManipulatorInterface
     */
    public function remove($line);

    /**
     * Change the given line number
     *
     * @param $line
     * @param $newContent
     * @return FileManipulatorInterface
     */
    public function change($line, $newContent);

    /**
     * Save the file
     */
    public function save();
}